package cn.ctodb.eadp.uaa.service.mapper;

import cn.ctodb.eadp.uaa.domain.*;
import cn.ctodb.eadp.uaa.service.dto.DeptDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity Dept and its DTO DeptDTO.
 */
@Mapper(componentModel = "spring", uses = {UserInfoMapper.class, })
public interface DeptMapper {

    @Mapping(source = "manager.id", target = "managerId")
    @Mapping(source = "parent.id", target = "parentId")
    DeptDTO deptToDeptDTO(Dept dept);

    List<DeptDTO> deptsToDeptDTOs(List<Dept> depts);

    @Mapping(target = "children", ignore = true)
    @Mapping(source = "managerId", target = "manager")
    @Mapping(source = "parentId", target = "parent")
    Dept deptDTOToDept(DeptDTO deptDTO);

    List<Dept> deptDTOsToDepts(List<DeptDTO> deptDTOs);
    /**
     * generating the fromId for all mappers if the databaseType is sql, as the class has relationship to it might need it, instead of
     * creating a new attribute to know if the entity has any relationship from some other entity
     *
     * @param id id of the entity
     * @return the entity instance
     */
     
    default Dept deptFromId(Long id) {
        if (id == null) {
            return null;
        }
        Dept dept = new Dept();
        dept.setId(id);
        return dept;
    }
    

}
