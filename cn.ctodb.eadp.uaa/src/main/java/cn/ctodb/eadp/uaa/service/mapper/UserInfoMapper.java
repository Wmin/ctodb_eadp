package cn.ctodb.eadp.uaa.service.mapper;

import cn.ctodb.eadp.uaa.domain.*;
import cn.ctodb.eadp.uaa.service.dto.UserInfoDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity UserInfo and its DTO UserInfoDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, })
public interface UserInfoMapper {

    @Mapping(source = "user.id", target = "userId")
    UserInfoDTO userInfoToUserInfoDTO(UserInfo userInfo);

    List<UserInfoDTO> userInfosToUserInfoDTOs(List<UserInfo> userInfos);

    @Mapping(source = "userId", target = "user")
    @Mapping(target = "depts", ignore = true)
    UserInfo userInfoDTOToUserInfo(UserInfoDTO userInfoDTO);

    List<UserInfo> userInfoDTOsToUserInfos(List<UserInfoDTO> userInfoDTOs);
    /**
     * generating the fromId for all mappers if the databaseType is sql, as the class has relationship to it might need it, instead of
     * creating a new attribute to know if the entity has any relationship from some other entity
     *
     * @param id id of the entity
     * @return the entity instance
     */
     
    default UserInfo userInfoFromId(Long id) {
        if (id == null) {
            return null;
        }
        UserInfo userInfo = new UserInfo();
        userInfo.setId(id);
        return userInfo;
    }
    

}
