package cn.ctodb.eadp.uaa.service.mapper;

import cn.ctodb.eadp.uaa.domain.*;
import cn.ctodb.eadp.uaa.service.dto.OrgDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity Org and its DTO OrgDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface OrgMapper {

    @Mapping(source = "parent.id", target = "parentId")
    OrgDTO orgToOrgDTO(Org org);

    List<OrgDTO> orgsToOrgDTOs(List<Org> orgs);

    @Mapping(target = "children", ignore = true)
    @Mapping(source = "parentId", target = "parent")
    Org orgDTOToOrg(OrgDTO orgDTO);

    List<Org> orgDTOsToOrgs(List<OrgDTO> orgDTOs);
    /**
     * generating the fromId for all mappers if the databaseType is sql, as the class has relationship to it might need it, instead of
     * creating a new attribute to know if the entity has any relationship from some other entity
     *
     * @param id id of the entity
     * @return the entity instance
     */
     
    default Org orgFromId(Long id) {
        if (id == null) {
            return null;
        }
        Org org = new Org();
        org.setId(id);
        return org;
    }
    

}
