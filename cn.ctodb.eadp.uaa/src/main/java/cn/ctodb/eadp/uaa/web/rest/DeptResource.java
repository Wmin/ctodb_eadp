package cn.ctodb.eadp.uaa.web.rest;

import com.codahale.metrics.annotation.Timed;
import cn.ctodb.eadp.uaa.service.DeptService;
import cn.ctodb.eadp.uaa.web.rest.util.HeaderUtil;
import cn.ctodb.eadp.uaa.web.rest.util.PaginationUtil;
import cn.ctodb.eadp.uaa.service.dto.DeptDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Dept.
 */
@RestController
@RequestMapping("/api")
public class DeptResource {

    private final Logger log = LoggerFactory.getLogger(DeptResource.class);

    private static final String ENTITY_NAME = "dept";
        
    private final DeptService deptService;

    public DeptResource(DeptService deptService) {
        this.deptService = deptService;
    }

    /**
     * POST  /depts : Create a new dept.
     *
     * @param deptDTO the deptDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new deptDTO, or with status 400 (Bad Request) if the dept has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/depts")
    @Timed
    public ResponseEntity<DeptDTO> createDept(@Valid @RequestBody DeptDTO deptDTO) throws URISyntaxException {
        log.debug("REST request to save Dept : {}", deptDTO);
        if (deptDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new dept cannot already have an ID")).body(null);
        }
        DeptDTO result = deptService.save(deptDTO);
        return ResponseEntity.created(new URI("/api/depts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /depts : Updates an existing dept.
     *
     * @param deptDTO the deptDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated deptDTO,
     * or with status 400 (Bad Request) if the deptDTO is not valid,
     * or with status 500 (Internal Server Error) if the deptDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/depts")
    @Timed
    public ResponseEntity<DeptDTO> updateDept(@Valid @RequestBody DeptDTO deptDTO) throws URISyntaxException {
        log.debug("REST request to update Dept : {}", deptDTO);
        if (deptDTO.getId() == null) {
            return createDept(deptDTO);
        }
        DeptDTO result = deptService.save(deptDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, deptDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /depts : get all the depts.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of depts in body
     */
    @GetMapping("/depts")
    @Timed
    public ResponseEntity<List<DeptDTO>> getAllDepts(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Depts");
        Page<DeptDTO> page = deptService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/depts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /depts/:id : get the "id" dept.
     *
     * @param id the id of the deptDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the deptDTO, or with status 404 (Not Found)
     */
    @GetMapping("/depts/{id}")
    @Timed
    public ResponseEntity<DeptDTO> getDept(@PathVariable Long id) {
        log.debug("REST request to get Dept : {}", id);
        DeptDTO deptDTO = deptService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(deptDTO));
    }

    /**
     * DELETE  /depts/:id : delete the "id" dept.
     *
     * @param id the id of the deptDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/depts/{id}")
    @Timed
    public ResponseEntity<Void> deleteDept(@PathVariable Long id) {
        log.debug("REST request to delete Dept : {}", id);
        deptService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/depts?query=:query : search for the dept corresponding
     * to the query.
     *
     * @param query the query of the dept search 
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/depts")
    @Timed
    public ResponseEntity<List<DeptDTO>> searchDepts(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of Depts for query {}", query);
        Page<DeptDTO> page = deptService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/depts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
