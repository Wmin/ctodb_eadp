/**
 * View Models used by Spring MVC REST controllers.
 */
package cn.ctodb.eadp.uaa.web.rest.vm;
