package cn.ctodb.eadp.uaa.repository.search;

import cn.ctodb.eadp.uaa.domain.Org;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Org entity.
 */
public interface OrgSearchRepository extends ElasticsearchRepository<Org, Long> {
}
