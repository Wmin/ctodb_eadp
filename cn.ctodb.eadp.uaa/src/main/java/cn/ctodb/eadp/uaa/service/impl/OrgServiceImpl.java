package cn.ctodb.eadp.uaa.service.impl;

import cn.ctodb.eadp.uaa.service.OrgService;
import cn.ctodb.eadp.uaa.domain.Org;
import cn.ctodb.eadp.uaa.repository.OrgRepository;
import cn.ctodb.eadp.uaa.repository.search.OrgSearchRepository;
import cn.ctodb.eadp.uaa.service.dto.OrgDTO;
import cn.ctodb.eadp.uaa.service.mapper.OrgMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Org.
 */
@Service
@Transactional
public class OrgServiceImpl implements OrgService{

    private final Logger log = LoggerFactory.getLogger(OrgServiceImpl.class);
    
    private final OrgRepository orgRepository;

    private final OrgMapper orgMapper;

    private final OrgSearchRepository orgSearchRepository;

    public OrgServiceImpl(OrgRepository orgRepository, OrgMapper orgMapper, OrgSearchRepository orgSearchRepository) {
        this.orgRepository = orgRepository;
        this.orgMapper = orgMapper;
        this.orgSearchRepository = orgSearchRepository;
    }

    /**
     * Save a org.
     *
     * @param orgDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public OrgDTO save(OrgDTO orgDTO) {
        log.debug("Request to save Org : {}", orgDTO);
        Org org = orgMapper.orgDTOToOrg(orgDTO);
        org = orgRepository.save(org);
        OrgDTO result = orgMapper.orgToOrgDTO(org);
        orgSearchRepository.save(org);
        return result;
    }

    /**
     *  Get all the orgs.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<OrgDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Orgs");
        Page<Org> result = orgRepository.findAll(pageable);
        return result.map(org -> orgMapper.orgToOrgDTO(org));
    }

    /**
     *  Get one org by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public OrgDTO findOne(Long id) {
        log.debug("Request to get Org : {}", id);
        Org org = orgRepository.findOne(id);
        OrgDTO orgDTO = orgMapper.orgToOrgDTO(org);
        return orgDTO;
    }

    /**
     *  Delete the  org by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Org : {}", id);
        orgRepository.delete(id);
        orgSearchRepository.delete(id);
    }

    /**
     * Search for the org corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<OrgDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Orgs for query {}", query);
        Page<Org> result = orgSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(org -> orgMapper.orgToOrgDTO(org));
    }
}
