package cn.ctodb.eadp.uaa.repository;

import cn.ctodb.eadp.uaa.domain.Org;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Org entity.
 */
@SuppressWarnings("unused")
public interface OrgRepository extends JpaRepository<Org,Long> {

}
