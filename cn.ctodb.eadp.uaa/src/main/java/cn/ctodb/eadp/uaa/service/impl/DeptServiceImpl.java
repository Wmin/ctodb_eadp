package cn.ctodb.eadp.uaa.service.impl;

import cn.ctodb.eadp.uaa.service.DeptService;
import cn.ctodb.eadp.uaa.domain.Dept;
import cn.ctodb.eadp.uaa.repository.DeptRepository;
import cn.ctodb.eadp.uaa.repository.search.DeptSearchRepository;
import cn.ctodb.eadp.uaa.service.dto.DeptDTO;
import cn.ctodb.eadp.uaa.service.mapper.DeptMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Dept.
 */
@Service
@Transactional
public class DeptServiceImpl implements DeptService{

    private final Logger log = LoggerFactory.getLogger(DeptServiceImpl.class);
    
    private final DeptRepository deptRepository;

    private final DeptMapper deptMapper;

    private final DeptSearchRepository deptSearchRepository;

    public DeptServiceImpl(DeptRepository deptRepository, DeptMapper deptMapper, DeptSearchRepository deptSearchRepository) {
        this.deptRepository = deptRepository;
        this.deptMapper = deptMapper;
        this.deptSearchRepository = deptSearchRepository;
    }

    /**
     * Save a dept.
     *
     * @param deptDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public DeptDTO save(DeptDTO deptDTO) {
        log.debug("Request to save Dept : {}", deptDTO);
        Dept dept = deptMapper.deptDTOToDept(deptDTO);
        dept = deptRepository.save(dept);
        DeptDTO result = deptMapper.deptToDeptDTO(dept);
        deptSearchRepository.save(dept);
        return result;
    }

    /**
     *  Get all the depts.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<DeptDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Depts");
        Page<Dept> result = deptRepository.findAll(pageable);
        return result.map(dept -> deptMapper.deptToDeptDTO(dept));
    }

    /**
     *  Get one dept by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public DeptDTO findOne(Long id) {
        log.debug("Request to get Dept : {}", id);
        Dept dept = deptRepository.findOneWithEagerRelationships(id);
        DeptDTO deptDTO = deptMapper.deptToDeptDTO(dept);
        return deptDTO;
    }

    /**
     *  Delete the  dept by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Dept : {}", id);
        deptRepository.delete(id);
        deptSearchRepository.delete(id);
    }

    /**
     * Search for the dept corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<DeptDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Depts for query {}", query);
        Page<Dept> result = deptSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(dept -> deptMapper.deptToDeptDTO(dept));
    }
}
