package cn.ctodb.eadp.uaa.repository.search;

import cn.ctodb.eadp.uaa.domain.Permission;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Permission entity.
 */
public interface PermissionSearchRepository extends ElasticsearchRepository<Permission, Long> {
}
