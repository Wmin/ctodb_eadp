package cn.ctodb.eadp.uaa.service.impl;

import cn.ctodb.eadp.uaa.service.PermissionService;
import cn.ctodb.eadp.uaa.domain.Permission;
import cn.ctodb.eadp.uaa.repository.PermissionRepository;
import cn.ctodb.eadp.uaa.repository.search.PermissionSearchRepository;
import cn.ctodb.eadp.uaa.service.dto.PermissionDTO;
import cn.ctodb.eadp.uaa.service.mapper.PermissionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Permission.
 */
@Service
@Transactional
public class PermissionServiceImpl implements PermissionService{

    private final Logger log = LoggerFactory.getLogger(PermissionServiceImpl.class);
    
    private final PermissionRepository permissionRepository;

    private final PermissionMapper permissionMapper;

    private final PermissionSearchRepository permissionSearchRepository;

    public PermissionServiceImpl(PermissionRepository permissionRepository, PermissionMapper permissionMapper, PermissionSearchRepository permissionSearchRepository) {
        this.permissionRepository = permissionRepository;
        this.permissionMapper = permissionMapper;
        this.permissionSearchRepository = permissionSearchRepository;
    }

    /**
     * Save a permission.
     *
     * @param permissionDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public PermissionDTO save(PermissionDTO permissionDTO) {
        log.debug("Request to save Permission : {}", permissionDTO);
        Permission permission = permissionMapper.permissionDTOToPermission(permissionDTO);
        permission = permissionRepository.save(permission);
        PermissionDTO result = permissionMapper.permissionToPermissionDTO(permission);
        permissionSearchRepository.save(permission);
        return result;
    }

    /**
     *  Get all the permissions.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PermissionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Permissions");
        Page<Permission> result = permissionRepository.findAll(pageable);
        return result.map(permission -> permissionMapper.permissionToPermissionDTO(permission));
    }

    /**
     *  Get one permission by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public PermissionDTO findOne(Long id) {
        log.debug("Request to get Permission : {}", id);
        Permission permission = permissionRepository.findOne(id);
        PermissionDTO permissionDTO = permissionMapper.permissionToPermissionDTO(permission);
        return permissionDTO;
    }

    /**
     *  Delete the  permission by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Permission : {}", id);
        permissionRepository.delete(id);
        permissionSearchRepository.delete(id);
    }

    /**
     * Search for the permission corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PermissionDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Permissions for query {}", query);
        Page<Permission> result = permissionSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(permission -> permissionMapper.permissionToPermissionDTO(permission));
    }
}
