package cn.ctodb.eadp.uaa.repository;

import cn.ctodb.eadp.uaa.domain.Dept;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the Dept entity.
 */
@SuppressWarnings("unused")
public interface DeptRepository extends JpaRepository<Dept,Long> {

    @Query("select distinct dept from Dept dept left join fetch dept.userInfos")
    List<Dept> findAllWithEagerRelationships();

    @Query("select dept from Dept dept left join fetch dept.userInfos where dept.id =:id")
    Dept findOneWithEagerRelationships(@Param("id") Long id);

}
