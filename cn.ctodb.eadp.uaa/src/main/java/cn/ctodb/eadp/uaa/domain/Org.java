package cn.ctodb.eadp.uaa.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * Entity Org
 */
@ApiModel(description = "Entity Org")
@Entity
@Table(name = "org")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "org")
public class Org implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "code", nullable = false)
    private String code;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "short_name")
    private String shortName;

    @Column(name = "description")
    private String description;

    @OneToMany(mappedBy = "parent")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Org> children = new HashSet<>();

    @ManyToOne
    private Org parent;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public Org code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public Org name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public Org shortName(String shortName) {
        this.shortName = shortName;
        return this;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getDescription() {
        return description;
    }

    public Org description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Org> getChildren() {
        return children;
    }

    public Org children(Set<Org> orgs) {
        this.children = orgs;
        return this;
    }

    public Org addChild(Org org) {
        this.children.add(org);
        org.setParent(this);
        return this;
    }

    public Org removeChild(Org org) {
        this.children.remove(org);
        org.setParent(null);
        return this;
    }

    public void setChildren(Set<Org> orgs) {
        this.children = orgs;
    }

    public Org getParent() {
        return parent;
    }

    public Org parent(Org org) {
        this.parent = org;
        return this;
    }

    public void setParent(Org org) {
        this.parent = org;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Org org = (Org) o;
        if (org.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, org.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Org{" +
            "id=" + id +
            ", code='" + code + "'" +
            ", name='" + name + "'" +
            ", shortName='" + shortName + "'" +
            ", description='" + description + "'" +
            '}';
    }
}
