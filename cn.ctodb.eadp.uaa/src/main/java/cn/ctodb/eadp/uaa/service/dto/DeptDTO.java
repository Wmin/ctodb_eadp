package cn.ctodb.eadp.uaa.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Dept entity.
 */
public class DeptDTO implements Serializable {

    private Long id;

    @NotNull
    private String code;

    @NotNull
    private String name;

    private String shortName;

    private String description;

    private Long managerId;

    private Set<UserInfoDTO> userInfos = new HashSet<>();

    private Long parentId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getManagerId() {
        return managerId;
    }

    public void setManagerId(Long userInfoId) {
        this.managerId = userInfoId;
    }

    public Set<UserInfoDTO> getUserInfos() {
        return userInfos;
    }

    public void setUserInfos(Set<UserInfoDTO> userInfos) {
        this.userInfos = userInfos;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long deptId) {
        this.parentId = deptId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DeptDTO deptDTO = (DeptDTO) o;

        if ( ! Objects.equals(id, deptDTO.id)) { return false; }

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "DeptDTO{" +
            "id=" + id +
            ", code='" + code + "'" +
            ", name='" + name + "'" +
            ", shortName='" + shortName + "'" +
            ", description='" + description + "'" +
            '}';
    }
}
