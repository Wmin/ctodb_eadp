package cn.ctodb.eadp.uaa.service;

import cn.ctodb.eadp.uaa.service.dto.OrgDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;

/**
 * Service Interface for managing Org.
 */
public interface OrgService {

    /**
     * Save a org.
     *
     * @param orgDTO the entity to save
     * @return the persisted entity
     */
    OrgDTO save(OrgDTO orgDTO);

    /**
     *  Get all the orgs.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<OrgDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" org.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    OrgDTO findOne(Long id);

    /**
     *  Delete the "id" org.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the org corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<OrgDTO> search(String query, Pageable pageable);
}
