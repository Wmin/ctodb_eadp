package cn.ctodb.eadp.uaa.repository.search;

import cn.ctodb.eadp.uaa.domain.Dept;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Dept entity.
 */
public interface DeptSearchRepository extends ElasticsearchRepository<Dept, Long> {
}
