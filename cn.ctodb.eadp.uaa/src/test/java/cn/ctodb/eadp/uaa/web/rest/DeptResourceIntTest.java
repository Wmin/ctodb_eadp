package cn.ctodb.eadp.uaa.web.rest;

import cn.ctodb.eadp.uaa.UaaApp;

import cn.ctodb.eadp.uaa.config.SecurityBeanOverrideConfiguration;

import cn.ctodb.eadp.uaa.domain.Dept;
import cn.ctodb.eadp.uaa.repository.DeptRepository;
import cn.ctodb.eadp.uaa.service.DeptService;
import cn.ctodb.eadp.uaa.repository.search.DeptSearchRepository;
import cn.ctodb.eadp.uaa.service.dto.DeptDTO;
import cn.ctodb.eadp.uaa.service.mapper.DeptMapper;
import cn.ctodb.eadp.uaa.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DeptResource REST controller.
 *
 * @see DeptResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = UaaApp.class)
public class DeptResourceIntTest {

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_SHORT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_SHORT_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private DeptRepository deptRepository;

    @Autowired
    private DeptMapper deptMapper;

    @Autowired
    private DeptService deptService;

    @Autowired
    private DeptSearchRepository deptSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDeptMockMvc;

    private Dept dept;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        DeptResource deptResource = new DeptResource(deptService);
        this.restDeptMockMvc = MockMvcBuilders.standaloneSetup(deptResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Dept createEntity(EntityManager em) {
        Dept dept = new Dept()
            .code(DEFAULT_CODE)
            .name(DEFAULT_NAME)
            .shortName(DEFAULT_SHORT_NAME)
            .description(DEFAULT_DESCRIPTION);
        return dept;
    }

    @Before
    public void initTest() {
        deptSearchRepository.deleteAll();
        dept = createEntity(em);
    }

    @Test
    @Transactional
    public void createDept() throws Exception {
        int databaseSizeBeforeCreate = deptRepository.findAll().size();

        // Create the Dept
        DeptDTO deptDTO = deptMapper.deptToDeptDTO(dept);
        restDeptMockMvc.perform(post("/api/depts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(deptDTO)))
            .andExpect(status().isCreated());

        // Validate the Dept in the database
        List<Dept> deptList = deptRepository.findAll();
        assertThat(deptList).hasSize(databaseSizeBeforeCreate + 1);
        Dept testDept = deptList.get(deptList.size() - 1);
        assertThat(testDept.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testDept.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testDept.getShortName()).isEqualTo(DEFAULT_SHORT_NAME);
        assertThat(testDept.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);

        // Validate the Dept in Elasticsearch
        Dept deptEs = deptSearchRepository.findOne(testDept.getId());
        assertThat(deptEs).isEqualToComparingFieldByField(testDept);
    }

    @Test
    @Transactional
    public void createDeptWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = deptRepository.findAll().size();

        // Create the Dept with an existing ID
        dept.setId(1L);
        DeptDTO deptDTO = deptMapper.deptToDeptDTO(dept);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDeptMockMvc.perform(post("/api/depts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(deptDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Dept> deptList = deptRepository.findAll();
        assertThat(deptList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = deptRepository.findAll().size();
        // set the field null
        dept.setCode(null);

        // Create the Dept, which fails.
        DeptDTO deptDTO = deptMapper.deptToDeptDTO(dept);

        restDeptMockMvc.perform(post("/api/depts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(deptDTO)))
            .andExpect(status().isBadRequest());

        List<Dept> deptList = deptRepository.findAll();
        assertThat(deptList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = deptRepository.findAll().size();
        // set the field null
        dept.setName(null);

        // Create the Dept, which fails.
        DeptDTO deptDTO = deptMapper.deptToDeptDTO(dept);

        restDeptMockMvc.perform(post("/api/depts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(deptDTO)))
            .andExpect(status().isBadRequest());

        List<Dept> deptList = deptRepository.findAll();
        assertThat(deptList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDepts() throws Exception {
        // Initialize the database
        deptRepository.saveAndFlush(dept);

        // Get all the deptList
        restDeptMockMvc.perform(get("/api/depts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dept.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].shortName").value(hasItem(DEFAULT_SHORT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getDept() throws Exception {
        // Initialize the database
        deptRepository.saveAndFlush(dept);

        // Get the dept
        restDeptMockMvc.perform(get("/api/depts/{id}", dept.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(dept.getId().intValue()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.shortName").value(DEFAULT_SHORT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingDept() throws Exception {
        // Get the dept
        restDeptMockMvc.perform(get("/api/depts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDept() throws Exception {
        // Initialize the database
        deptRepository.saveAndFlush(dept);
        deptSearchRepository.save(dept);
        int databaseSizeBeforeUpdate = deptRepository.findAll().size();

        // Update the dept
        Dept updatedDept = deptRepository.findOne(dept.getId());
        updatedDept
            .code(UPDATED_CODE)
            .name(UPDATED_NAME)
            .shortName(UPDATED_SHORT_NAME)
            .description(UPDATED_DESCRIPTION);
        DeptDTO deptDTO = deptMapper.deptToDeptDTO(updatedDept);

        restDeptMockMvc.perform(put("/api/depts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(deptDTO)))
            .andExpect(status().isOk());

        // Validate the Dept in the database
        List<Dept> deptList = deptRepository.findAll();
        assertThat(deptList).hasSize(databaseSizeBeforeUpdate);
        Dept testDept = deptList.get(deptList.size() - 1);
        assertThat(testDept.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testDept.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testDept.getShortName()).isEqualTo(UPDATED_SHORT_NAME);
        assertThat(testDept.getDescription()).isEqualTo(UPDATED_DESCRIPTION);

        // Validate the Dept in Elasticsearch
        Dept deptEs = deptSearchRepository.findOne(testDept.getId());
        assertThat(deptEs).isEqualToComparingFieldByField(testDept);
    }

    @Test
    @Transactional
    public void updateNonExistingDept() throws Exception {
        int databaseSizeBeforeUpdate = deptRepository.findAll().size();

        // Create the Dept
        DeptDTO deptDTO = deptMapper.deptToDeptDTO(dept);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restDeptMockMvc.perform(put("/api/depts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(deptDTO)))
            .andExpect(status().isCreated());

        // Validate the Dept in the database
        List<Dept> deptList = deptRepository.findAll();
        assertThat(deptList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteDept() throws Exception {
        // Initialize the database
        deptRepository.saveAndFlush(dept);
        deptSearchRepository.save(dept);
        int databaseSizeBeforeDelete = deptRepository.findAll().size();

        // Get the dept
        restDeptMockMvc.perform(delete("/api/depts/{id}", dept.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean deptExistsInEs = deptSearchRepository.exists(dept.getId());
        assertThat(deptExistsInEs).isFalse();

        // Validate the database is empty
        List<Dept> deptList = deptRepository.findAll();
        assertThat(deptList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchDept() throws Exception {
        // Initialize the database
        deptRepository.saveAndFlush(dept);
        deptSearchRepository.save(dept);

        // Search the dept
        restDeptMockMvc.perform(get("/api/_search/depts?query=id:" + dept.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dept.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].shortName").value(hasItem(DEFAULT_SHORT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Dept.class);
    }
}
