/**
 * Data Access Objects used by WebSocket services.
 */
package cn.ctodb.eadp.gateway.web.websocket.dto;
