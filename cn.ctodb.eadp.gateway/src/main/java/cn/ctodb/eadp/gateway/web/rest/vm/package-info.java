/**
 * View Models used by Spring MVC REST controllers.
 */
package cn.ctodb.eadp.gateway.web.rest.vm;
