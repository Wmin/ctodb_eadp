#ctodb_eadp 企业应用开发平台

## 模块
工程 | 说明
---|---
cn.ctodb.eadp.registry | 注册服务
cn.ctodb.eadp.uaa | 授权服务 
cn.ctodb.eadp.gateway | 网关

## 技术栈
### server
- Spring Boot
- Spring Security
- Hibernate
- MySQL
- Hazelcast
- ElasticSearch
- Swagger
- Thymeleaf
- Maven


### client
- HTML5
- CSS3
- vue
- Webpack
- Websockets


### deploy
- Docker